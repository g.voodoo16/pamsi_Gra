/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File: Array2D.h
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once

namespace pamsi {

    template <typename T>
    class Array2D{
    public:
        /**************************************************************************************************************
         * DEFAULT CONSTRUCTOR
         *************************************************************************************************************/
        Array2D<T>( unsigned int _width, unsigned int _height ): _width(_width), _height(_height), _content(nullptr){
            _content = new T[_width * _height];

        }

        /**************************************************************************************************************
        * DESTRUCTOR
        **************************************************************************************************************/
        virtual ~Array2D<T>(){
            delete[] _content;
        }

        /**************************************************************************************************************
        * COPY CONSTRUCTOR
        **************************************************************************************************************/
        Array2D<T>( const Array2D<T> &obj ){
            _height = obj._height;
            _width = obj._width;

            _content = new T[obj._width * obj._height];
            for( unsigned int i = 0; i < obj._width; ++i ){
                for( unsigned int j = 0; j < obj._height; ++j ){
                    this->at(i, j) = obj.at(i, j);
                }
            }
        }

        /**************************************************************************************************************
        * MOVE CONSTRUCTOR
        **************************************************************************************************************/
        Array2D<T>( Array2D<T> &&obj ): _content(obj._content){
            obj._content = nullptr;
        }

        /**************************************************************************************************************
        * COPY ASSIGNMENT OPERATOR
        **************************************************************************************************************/
        Array2D<T> &operator=( const Array2D<T> &obj ){
            T *temp = new T[obj._width * obj._height];
            for( unsigned int i = 0; i < obj._width; ++i ){
                for( unsigned int j = 0; j < obj._height; j++ ){
                    temp[i * _height + j] = obj.at(i, j);
                }
            }
            delete[] _content;
            _content = temp;
            return *this;
        }

        /**************************************************************************************************************
        * MOVE ASSIGNMENT OPERATOR
        **************************************************************************************************************/
        Array2D<T> &operator=( Array2D<T> &&obj ){
            delete[] _content;
            _content = obj._content;
            obj._content = nullptr;
            return *this;
        }

        /**************************************************************************************************************
        * ACCESS METHODS
        **************************************************************************************************************/
        T &at( unsigned int row, unsigned int col ){
            return _content[row * _height + col];
        }

        const T &at( unsigned int row, unsigned int col ) const{
            return _content[row * _height + col];
        }

        T &operator()( unsigned int row, unsigned int col ){
            return _content[row * _height + col];
        }

        const T &operator()( unsigned int row, unsigned int col ) const{
            return _content[row * _height + col];
        }

        unsigned int get_width() const{
            return _width;
        }

        unsigned int get_height() const{
            return _height;
        }

    private:
        unsigned int _width;
        unsigned int _height;
        T *_content;
    };


}