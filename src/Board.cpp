/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File: Board.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "Board.h"


void Board::updateTile( unsigned int row, unsigned int col, Tile *tile ){
    // usuwanie poprzedniego kafelka
    if( _board->at(row, col) != nullptr )
        delete _board->at(row, col);

    _board->at(row, col) = tile;
    if( tile->get_type() != TileType::EMPTY )
        _emptyLeft--;
    else
        _emptyLeft++;
}

std::vector<std::pair<unsigned int, unsigned int>> Board::findAllEmpty() const{
    std::vector<std::pair<unsigned int, unsigned int> > temp;

    for( unsigned int i = 0; i < _width; ++i ){
        for( unsigned int j = 0; j < _height; ++j ){
            if( _board->at(i, j)->get_type() == TileType::EMPTY ){
                std::pair<unsigned int, unsigned int> pair;
                pair.first = i;
                pair.second = j;
                temp.push_back(pair);
            }
        }
    }
    return temp;
}

TileType Board::hasWinner() const{
    // TODO: SKROC TO KURWA DEBILU!
    unsigned int counterX = 0; // zlicza ilosc pod rzad
    unsigned int counterO = 0;
    TileType x = TileType::X;
    TileType o = TileType::O;

    // sprawdzanie w wierszach
    for( unsigned int i = 0; i < _height; ++i ){
        for( unsigned int j = 0; j <= _width - _winRowN; ++j ){
            // resetowanie dla kazdego sprawdzania
            counterX = 0;
            counterO = 0;
            for( unsigned int k = 0; k < _winRowN; ++k ){
                const TileType &type = _board->at(i, j + k)->get_type();
                if( type == TileType::X ){
                    counterX++;
                    if( counterX == _winRowN )
                        return x;
                } else if( type == TileType::O ){
                    counterO++;
                    if( counterO == _winRowN )
                        return o;
                } else{
                    break;
                }
            }
        }
    }

    // sprawdzanie w kolumnach
    for( unsigned int i = 0; i < _width; ++i ){
        for( unsigned int j = 0; j <= _height - _winRowN; ++j ){
            // resetowanie dla kazdego sprawdzania
            counterX = 0;
            counterO = 0;
            for( unsigned int k = 0; k < _winRowN; ++k ){
                const TileType &type = _board->at(j + k, i)->get_type();
                if( type == TileType::X ){
                    counterX++;
                    if( counterX == _winRowN )
                        return TileType::X;
                } else if( type == TileType::O ){
                    counterO++;
                    if( counterO == _winRowN )
                        return TileType::O;
                } else{
                    break;
                }
            }
        }
    }

    // sprawdzanie diagonalnie
    for( unsigned int i = 0; i <= _height - _winRowN; ++i ){
        for( unsigned int j = 0; j <= _width - _winRowN; ++j ){
            // resetowanie dla kazdego sprawdzania
            counterX = 0;
            counterO = 0;
            for( unsigned int k = 0; k < _winRowN; ++k ){
                const TileType &type = _board->at(j + k, i + k)->get_type();
                if( type == TileType::X ){
                    counterX++;
                    if( counterX == _winRowN )
                        return TileType::X;
                } else if( type == TileType::O ){
                    counterO++;
                    if( counterO == _winRowN )
                        return TileType::O;
                } else{
                    break;
                }
            }
        }
    }

    // jezeli ani X ani O nie wygralo to zwracamy
    // puste pole jako remix
    return TileType::EMPTY;

}

void Board::print() const{

    // wypisanie numerow kolumn
    std::cout << " \t  ";
    for( unsigned int i = 0; i < _width; ++i ){
        std::cout << i << "   ";
    }
    std::cout << std::endl;

    // wypisanie zawartosci
    for( unsigned int i = 0; i < _width; ++i ){
        std::cout << i << "\t|";
        for( unsigned int j = 0; j < _height; ++j ){
            std::cout << " ";
            _board->at(i, j)->print();
            std::cout << " |";
        }
        std::cout << std::endl;
    }

}