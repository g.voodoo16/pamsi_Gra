/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File: Board.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once


#include "Vector2D.h"
#include "Tile.h"
#include "TileEmpty.h"
#include "TileX.h"
#include "TileO.h"
#include "Array2D.h"

class Board{

public:
    /*******************************************************************************************************************
     * RULE--OF--FIVE:
     * - DESTRUCTOR,
     * - COPY CONSTRUCTOR,
     * - COPY ASSIGNMENT OPERATOR,
     * - MOVE CONSTRUCTOR,
     * - MOVE ASSIGNMENT OPERATOR
     ******************************************************************************************************************/
    /*******************************************************************************************************************
    * DEFAULT CONSTRUCTOR
    *******************************************************************************************************************/
    Board( unsigned int width ): _width(width), _height(width), _winRowN(0), _emptyLeft(width * width), _board(nullptr){
        _board = new pamsi::Array2D<Tile *>(_width, _height);

        for( unsigned int i = 0; i < _height; ++i ){
            for( unsigned int j = 0; j < _width; ++j ){
                _board->at(i, j) = new TileEmpty();
            }
        }

    }

    /*******************************************************************************************************************
     * DESTRUCTOR
     ******************************************************************************************************************/
    virtual ~Board(){
        for( unsigned int i = 0; i < _height; ++i )
            for( unsigned int j = 0; j < _width; ++j )
                delete _board->at(i, j);
        delete _board;
    }

    /*******************************************************************************************************************
     * COPY CONSTRUCTOR
     ******************************************************************************************************************/
    Board( const Board &brd ){
        this->_width = brd._width;
        this->_height = brd._height;
        this->_winRowN = brd._winRowN;
        this->_emptyLeft = brd._emptyLeft;

        _board = new pamsi::Array2D<Tile *>(_height, _width);
        for( unsigned int i = 0; i < _height; ++i ){
            for( unsigned int j = 0; j < _width; ++j ){
                if( brd.getTile(i, j)->get_type() == TileType::X )
                    _board->at(i, j) = new TileX();
                else if( brd.getTile(i, j)->get_type() == TileType::O )
                    _board->at(i, j) = new TileO();
                else
                    _board->at(i, j) = new TileEmpty();
            }
        }
    }

    /*******************************************************************************************************************
     * MOVE CONSTRUCTOR
     ******************************************************************************************************************/
    Board( Board &&brd ): _board(brd._board){
        brd._board = nullptr;
    }

    /*******************************************************************************************************************
    * COPY ASSIGNMENT OPERATOR
    *******************************************************************************************************************/
    Board &operator=( const Board &brd ){
        this->_width = brd._width;
        this->_height = brd._height;
        this->_winRowN = brd._winRowN;
        this->_emptyLeft = brd._emptyLeft;

        auto *temp = new pamsi::Array2D<Tile *>(_height, _width);
        for( unsigned int i = 0; i < _height; ++i ){
            for( unsigned int j = 0; j < _width; ++j ){
                if( brd.getTile(i, j)->get_type() == TileType::X )
                    temp->at(i, j) = new TileX();
                else if( brd.getTile(i, j)->get_type() == TileType::O )
                    temp->at(i, j) = new TileO();
                else
                    temp->at(i, j) = new TileEmpty();
            }
        }
        for( unsigned int i = 0; i < _height; ++i )
            for( unsigned int j = 0; j < _width; ++j )
                delete _board->at(i, j);
        delete _board;
        _board = temp;
        return *this;
    }

    /*******************************************************************************************************************
    * MOVE ASSIGNMENT OPERATOR
    *******************************************************************************************************************/
    Board &operator=( Board &&brd ){
        for( unsigned int i = 0; i < _height; ++i )
            for( unsigned int j = 0; j < _width; ++j )
                delete _board->at(i, j);
        _board = brd._board;
        brd._board = nullptr;
        return *this;
    }

    /*******************************************************************************************************************
    * ACCESS METHODS
    *******************************************************************************************************************/
    unsigned int get_width() const{
        return _width;
    }

    unsigned int get_height() const{
        return _height;
    }

    unsigned int get_winRowN() const{
        return _winRowN;
    }


    unsigned int get_emptyLeft() const{
        return _emptyLeft;
    }

    void set_winRowN( unsigned int _winRowN ){
        Board::_winRowN = _winRowN;
    }

    const Tile *getTile( unsigned int row, unsigned int col ) const{
        return _board->at(row, col);
    }

    /*******************************************************************************************************************
    * MAIN METHODS
    *******************************************************************************************************************/
    void updateTile( unsigned int row, unsigned int col, Tile *tile );

    std::vector<std::pair<unsigned int, unsigned int>> findAllEmpty() const;

    TileType hasWinner() const;

    void print() const;


private:
    unsigned int _width;
    unsigned int _height;
    unsigned int _winRowN;
    unsigned int _emptyLeft;
    pamsi::Array2D<Tile *> *_board;

};


