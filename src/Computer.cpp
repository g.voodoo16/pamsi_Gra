/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: Computer.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "Computer.h"

void Computer::makeMove(){

    // odpalamy algorytm minimax
    int depth = get_board()->get_emptyLeft();
    int max_depth = depth - _iq + 1;
    // zabezpieczenie dla maksymalnej glebokosci
    if( max_depth < 1 ){
        max_depth = 1;
    }
    minimax(depth, max_depth, *get_board());
}

TileType Computer::minimax( int depth, int max_depth, const Board &brd ){
    /******************************************************************************************************************
     * TWORZENIE WSZYSTKICH MOZLIWYCH POZIOMOW NA DANEJ GLEBOKOSCI
     *****************************************************************************************************************/
    // tworzy n kopii planszy wejsciowej, gdzie n to glebokosc
    std::vector<Board> boards;
    for( unsigned int i = 0; i < depth; ++i ){
        Board newBrd(brd.get_height());
        newBrd = brd;
        boards.push_back(newBrd);
    }

    // tworzy vector z wszystkimi wolnymi polami
    auto empty = brd.findAllEmpty();

    // Tworzy poziomy, dla kazdego wolnego pola wstawiamy ruch PC lub GRACZa
    int isPlayerTurn = brd.get_emptyLeft() % 2;


    if(!isPlayerTurn){ // ruchy dla GRACZa
        for( unsigned int i = 0; i < brd.get_emptyLeft(); ++i ){
            boards[i].updateTile(empty[i].first, empty[i].second, new TileX());
        }
    } else{ // ruchy dla KOMPUTERa
        for( unsigned int i = 0; i < brd.get_emptyLeft(); ++i ){
            boards[i].updateTile(empty[i].first, empty[i].second, new TileO());
        }
    }

    /******************************************************************************************************************
     * ODPALANIE REKURENCJI I ZBIERANIE WYNIKOW
     *****************************************************************************************************************/
    TileType *result = new TileType[depth];

    // dla kazdej planszy na poziomie szukamy zwyciezcy
    for( unsigned int i = 0; i < boards.size(); ++i ){
        TileType winner = boards[i].hasWinner();

        if( static_cast<int>(winner) != 0 ){ // jezeli na danej planszy jest zwyciezca, zwroc go
            result[i] = winner;
        } else if( depth == max_depth ){ // jezeli doszlismy do maksymalnej glebokosci
            result[i] = winner;
        } else{
            result[i] = minimax(depth - 1, max_depth, boards[i]);
        }
    }

    /******************************************************************************************************************
     * SZUKANIE EKSTREMUM (MIN lub MAX) NA DANYM POZIOMIE
     *****************************************************************************************************************/
    TileType extr = result[0]; // nie ma prawa wyjsc na minus z indeksem
    int extr_index = 0;
    // KROK MIN
    if( isPlayerTurn ){
        for( unsigned int i = 1; i < depth; ++i ){
            if( static_cast<int>(extr) >= static_cast<int>(result[i]) ){
                extr = result[i];
                extr_index = i;
            }
        }
    } else{ // KROK MAX
        for( unsigned int i = 1; i < depth; ++i ){
            if( static_cast<int>(extr) <= static_cast<int>(result[i]) ){
                extr = result[i];
                extr_index = i;
            }
        }
    }
    delete[] result;
    /******************************************************************************************************************
     * WYKONYWANIE RUCHU GDY JESTESMY NA SAMEJ GORZE ZNOW
     *****************************************************************************************************************/
    if(get_board()->get_emptyLeft() == depth){
        get_board()->updateTile(empty[extr_index].first, empty[extr_index].second, new TileO());
    }

    /******************************************************************************************************************
     * ZWRACANIE EKSTREMUM WYNIKU
     *****************************************************************************************************************/
    return extr;
}

