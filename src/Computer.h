/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: Computer.h
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once

#include <string>
#include "TileEmpty.h"
#include "Player.h"

class Computer : public Player{

public:
    Computer(std::string name, bool turn, Tile *weapon): _iq(1){
        initialize(name, turn, weapon);
    }


    unsigned int get_iq() const{
        return _iq;
    }


    void set_iq( unsigned int _iq ){
        Computer::_iq = _iq;
    }


    virtual void makeMove();

private:
    unsigned int _iq;

    TileType minimax( int depth, int max_depth, const Board &brd );

    TileType minimax_bak( int depth, int max_depth, const Board &brd );
};



