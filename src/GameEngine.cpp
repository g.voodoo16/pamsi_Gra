/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: GameEngine.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <string.h>
#include "GameEngine.h"
#include "Human.h"
#include "Computer.h"

void GameEngine::initialize(){
    // zmienne pomocnicze z wartosciami domyslnymi
    const char *const delimLine = "********************************************************************************";
    const char *const gameTitle = "*************************G A M E S O F C R O S S E S****************************";
    const char *const broName = "Ziomek";
    std::string playerName = "Nieznajomy";
    std::string attack;
    unsigned int iq;
    unsigned int n;
    unsigned int max;
    Computer *pc;
    Human *human;

    Tile *o = new TileO();
    TileX *x = new TileX();

    std::cout << delimLine << std::endl;
    std::cout << gameTitle << std::endl;
    std::cout << delimLine << std::endl;
    std::string text = "W pelnij automatyczny ziomeczek przeprowadzi konfiguracje gry...";
    sendText("...", text, LINEOUT);
    text = "Witaj nieznajomy w świecie krzyży! Jak sie nazywasz?";
    sendText(broName, text, LINEOUT);

    std::cout << playerName << ": ";
    std::cin >> playerName;
    std::cout << std::endl;

    text = "Bardzo dziwne imie ";
    text += playerName;
    text += ", bardzo dziwne. Tak poprawdzie nawet twoj wyglad sugeruje, ze niepochodzisz stad. Gdziez sie podzialy ";
    text += "moje maniery! Jestem Kross, glowny dowodca wojsk ksiestwa Krzyzy w krainie Pamsi. Obecnie przeprowadzam ";
    text += "zwiad w poszukiwaniu zagrozen i rekrutowania nowych zolnierzy. Wygladasz mi na takiego, ktory poszukuje ";
    text += "slawy...";
    sendText("Kross", text, LINEOUT);

    text = "Przybyles tutaj aby nas zaatakowac czy aby nas wesprzec swoim orezem?";
    sendText("Kross", text, LINEOUT);

    text = "\t (1) - Zaatakuj (2) - Dolacz";
    sendText("", text, LINEOUT);
    std::cout << playerName << ": ";
    std::cin >> attack;
    if( attack == "1" ){ // atak
        // konfiguracja czlowieka
        human = new Human(playerName, false, o);

        // konfiguracja komputera
        pc = new Computer("Kross", false, x);

        // dalsze zadawanie pytan
        std::cout << "Kross: A wiec to tak! Przygotuj sie do walki nikczemniku! Masz szczescie, ze jestesmy \n"
        << "ludem bardzo honorowym i pozwolimy Ci wybrac reguly walki jako ze jestes u nas gosciem!"
        << std::endl;

    } else{ // obrona
        // konfiguracja gracza
        human = new Human(playerName, false, x);

        // konfiguracja komputera
        pc = new Computer("Nots", false, o);

        // dalsze zadawanie pytan
        std::cout << "Kross: Swietnie! Masz szanse od razu wykazac sie zdolnosciami rozpoznawczymi!" << std::endl;
        std::cout << "Kross: Za tamta wydma znajduje sie nasz wrog - potezny Nots. Ananas jakich malo! \n"
        << " Ciagle kradnie nam kozy i podrzuca swinie. Idz tam i ustal warunki bitwy!"
        << std::endl;
    }

    // ustawienie wymiarow planszy
    std::cout << "Wymiary planszy (n x n)" << std::endl;
    std::cout << "n = ";
    std::cin >> n;
    _board = new Board(n);

    // ustawienie wymiarow planszy
    std::cout << "Ile znakow jest sekwencja wygrywajaca? (1-" << n << ")" << std::endl;
    std::cout << "wygrywajaca sekwencja = ";
    std::cin >> max;
    _board->set_winRowN(max);

    // ustawienie poziomu inteligencji
    std::cout << "Poziom inteligencji 1-10" << std::endl;
    std::cout << playerName << ": ";
    std::cin >> iq;
    pc->set_iq(iq);

    // przypisanie graczy
    _player = human;
    _pc = pc;

    _player->set_board(_board);
    _pc->set_board(_board);
}

void GameEngine::startGame(){
    //initialBoard();
    TileType winner = _board->hasWinner();
    int movesLeft = 0;
    print();
    while( !isEnd ){
        movesLeft = _board->get_emptyLeft();

        if( movesLeft == 0 ){
            isEnd = true;
        } else if( ( movesLeft % 2 ) == 0 ){
            _pc->makeMove();
        } else{
            _player->makeMove();
        }
        print();

        winner = _board->hasWinner();
        if( static_cast<int>(winner) != 0)
            isEnd = true;
    }

    if( winner == TileType::O ){
        std::cout << "!!! PC is WINNER !!!" << std::endl;
    } else if( winner == TileType::X ){
        std::cout << "!!! WINNER is WINNER !!!" << std::endl;
    } else{
        std::cout << "!!! DEUCE !!!" << std::endl;
    }

    return;
}

void GameEngine::print() const{
    const char *const delimLine = "********************************************************************************";
    std::cout << delimLine << std::endl;
    std::cout << "(" << _player->get_weapon()->get_printMsg() << ") " << _player->get_name();
    std::cout << "\t VS \t";
    std::cout << _pc->get_name() << " (" << _pc->get_weapon()->get_printMsg() << ")" << std::endl;
    std::cout << delimLine << std::endl;

    _board->print();
}

void GameEngine::sendText( std::string charName, std::string msg, unsigned int size ) const{
    unsigned int spaces = charName.size() + 2;
    unsigned int total = 0;

    do{
        // w poszukiwaniu spacji od konca
        unsigned int offset = 0;
        if( msg[size + total] == ' ' ){
            for( int i = ( total + size - spaces ); i > total; --i ){
                if( msg[i] != ' ' )
                    offset++;
                else
                    break;
            }
        }

        // jezeli nie ma spacji
        if( offset == ( size - spaces ) )
            offset = 0;

        // wypisywanie dla pierwszej lini
        if( total == 0 )
            std::cout << charName << ": ";
        else{
            for( int j = 0; j < spaces; ++j ){
                std::cout << " ";
            }
        }

        // wypisywanie msg na stdout
        unsigned int end = total + size - offset - spaces;

        // zabezpieczenie przed wyjechaniem za stringa
        if( end > msg.size() )
            end = msg.size();
        for( int k = total; k < end; ++k ){
            std::cout << msg.at(k);
            total++;
        }
        std::cout << std::endl;
    } while( total < msg.size() );
}

void GameEngine::fast_init(){
    // zmienne pomocnicze z wartosciami domyslnymi
    const char *const broName = "PC";
    std::string playerName = "GRACZ";
    unsigned int iq = 4;
    unsigned int n = 3;
    unsigned int max = 3;
    Computer *pc;
    Human *human;

    Tile *o = new TileO();
    TileX *x = new TileX();

    // konfiguracja czlowieka
    human = new Human(playerName, false, x);
    // konfiguracja komputera
    pc = new Computer("Kross", false, o);
    // ustawienie wymiarow planszy
    _board = new Board(n);
    // ustawienie wymiarow planszy
    _board->set_winRowN(max);
    // ustawienie poziomu inteligencji
    pc->set_iq(iq);
    // przypisanie graczy
    _player = human;
    _pc = pc;

    _player->set_board(_board);
    _pc->set_board(_board);

}

void GameEngine::initialBoard(){
    // ustawienie pierwszych 5 ruchow
    _board->updateTile(0, 0, new TileO());
    _board->updateTile(2, 0, new TileO());
    _board->updateTile(0, 2, new TileX());
    _board->updateTile(1, 0, new TileX());
    // _board->updateTile(1, 1, new TileX());

}
