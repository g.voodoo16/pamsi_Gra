/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: GameEngine.h
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once


#include "Board.h"
#include "Player.h"

static const int LINEOUT = 120;

class GameEngine{


public:
    GameEngine(): _board(NULL), _player(NULL), _pc(NULL){
        isEnd = false;
    }

    void initialize();
    void fast_init();
    void startGame();
    void initialBoard();

private:
    Board *_board;
    Player *_player;
    Player *_pc;
    bool isEnd;

    void sendText(std::string charName, std::string msg, unsigned int size) const;
    void print() const;

};



