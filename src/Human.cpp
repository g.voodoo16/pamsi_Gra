/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: Human.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "Human.h"

void Human::makeMove(){
    unsigned int row;
    unsigned int col;
    bool valid = false;

    while(!valid){
        std::cout << get_name() << " (wiersz kolumna): ";
        std::cin >> row >> col;
        std::cout << std::endl;

        valid = validateMove(row, col);
        if(!valid)
            std::cout << "Niepoprawny ruch! Sprobuj ponownie." << std::endl;
    }

    get_board()->updateTile(row, col, get_weapon());

}
