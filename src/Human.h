/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: Human.h
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once

#include <string>
#include "TileType.h"
#include "Player.h"

class Human : public Player{

public:
    Human(std::string name, bool turn, Tile *weapon){
        initialize(name, turn, weapon);
    }


    virtual void makeMove();
};



