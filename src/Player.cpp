/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: Player.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "Player.h"

void Player::initialize( std::string name, bool turn, Tile *weapon ){
    _name = name;
    _turn = turn;
    _weapon = weapon;
    _wpn = _weapon->get_type();
    _board = NULL;
}

bool Player::validateMove( unsigned int row, unsigned int col ){
    if(row > get_board()->get_height())
        return false;
    if(col > get_board()->get_width())
        return false;
    if(row < 0)
        return false;
    if(col < 0)
        return false;

    if(get_board()->getTile(row, col)->get_type() != TileType::EMPTY)
        return false;

    return true;
}
