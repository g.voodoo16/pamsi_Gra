/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * File: Player.h
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once

#include <string>
#include "Tile.h"
#include "Board.h"

class Player{


public:

    virtual ~Player(){
        delete _weapon;
    }

    const std::string &get_name() const{
        return _name;
    }

    bool is_turn() const{
        return _turn;
    }


    Tile *get_weapon() const{
        return _weapon;
    }

    const TileType &get_wpn() const{
        return _wpn;
    }

    Board *get_board() const{
        return _board;
    }

    void set_board( Board *_board ){
        Player::_board = _board;
    }

    virtual void makeMove() = 0;

    bool validateMove( unsigned int row, unsigned int col );

protected:
    void set_name( const std::string &_name ){
        Player::_name = _name;
    }

    void set_turn( bool _turn ){
        Player::_turn = _turn;
    }

    void set_weapon( Tile *_weapon ){
        Player::_weapon = _weapon;
    }

    void set_wpn( const TileType &_wpn ){
        Player::_wpn = _wpn;
    }

    void initialize( std::string name, bool turn, Tile *weapon );

private:
    std::string _name;
    bool _turn;
    TileType _wpn;
    Tile *_weapon;
    Board *_board;
};



