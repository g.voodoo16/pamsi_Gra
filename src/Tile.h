/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File: test.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once


#include <string>
#include "TileType.h"

class Tile {
public:

/*
    Tile(){
        //this = new TileEmpty();
    }
*/

    const TileType &get_type() const {
        return _type;
    }

    const std::string &get_printMsg() const {
        return _printMsg;
    }

    void print() const;
protected:
    void set_type(const TileType &_type) {
        Tile::_type = _type;
    }

    void set_printMsg(const std::string &_printMsg) {
        Tile::_printMsg = _printMsg;
    }

private:
    TileType _type;
    std::string _printMsg;
    virtual void initialize() = 0;

};

