/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File: test.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once

#include <iostream>
#include <vector>

namespace pamsi {

/*!
 * Klasa modeluje pojecie macierzy kwadratowej. Rozszerzanie w O(n) ;C
 */
    template<typename T>
    class Vector2D{
    public:

        Vector2D(){
            _vector.resize(1);
            _vector[0].resize(1);
        };

        Vector2D(unsigned int n){
            _vector.resize(n);
            for(unsigned int i = 0; i < n; ++i){
                _vector[i].resize(n);
            }
        }
        /*!
         *
         * @return ilosc wierszy
         */
        unsigned int size() const;

        /*!
         * Zwraca zawartosc pod zadana pozycja.
         * @param row numer wiersza.
         * @param col numer kolumny.
         * @return zawartoc pod zadana pozycja.
         */
        const T& get(unsigned int row, unsigned int col) const;

        /*!
         * Ustawia zadana wartosc pod zadana pozycja.
         * @param row numer wiersza.
         * @param col numer kolumny.
         * @param value wartosc.
         */
        void set(unsigned int row, unsigned int col, const T& value);

        /*!
         * Zwraca zawartosc pod zadana pozycja.
         * @param row numer wiersza.
         * @param col numer kolumny.
         * @return zawartoc pod zadana pozycja.
         */
        const T& operator()(unsigned int row, unsigned int col) const;
        /*!
         * Ustawia zadana wartosc pod zadana pozycja.
         * @param row numer wiersza.
         * @param col numer kolumny.
         */
        T& operator()(unsigned int row, unsigned int col);

        /*!
         * Zwieksza rozmiar macierzy o 1 wiersz oraz 1 kolumne.
         */
        void resize();

        /*!
         *
         */
        void clear();

        /*!
         * Wypisuje macierz na std.
         */
        void print() const;

    private:
        std::vector<std::vector<T>> _vector; /*!< przechowuje macierz n x n. */
    };

    template<typename T>
    unsigned int pamsi::Vector2D<T>::size() const{
        return _vector[0].size();
    }

    template<typename T>
    const T& pamsi::Vector2D<T>::get(unsigned int row, unsigned int col) const{
        return _vector[row][col];
    }

    template<typename T>
    void pamsi::Vector2D<T>::set(unsigned int row, unsigned int col,
                                 const T &value){
        _vector[row][col] = value;
    }

    template<typename T>
    void pamsi::Vector2D<T>::resize(){
        unsigned int size = _vector.size();

        // dodawanie kolumn
        for(unsigned int i = 0; i < size; ++i){
            _vector[i].resize(size + 1);
        }
        // dodawanie wiersza
        std::vector<T> row;
        row.resize(size + 1);
        _vector.push_back(row);
    }

    template<typename T>
    const T& pamsi::Vector2D<T>::operator()(unsigned int row,
                                            unsigned int col) const{
        return _vector[row][col];
    }

    template<typename T>
    T& pamsi::Vector2D<T>::operator()(unsigned int row, unsigned int col
    ){
        return _vector[row][col];
    }

    template<typename T>
    void pamsi::Vector2D<T>::clear(){
        unsigned int size = _vector[0].size();

        for(unsigned int i = 0; i < size; ++i)
            for(unsigned int j = 0; j < size; ++j)
                _vector[i].pop_back();

    }

    template<typename T>
    void pamsi::Vector2D<T>::print() const{
        for(unsigned int i = 0; i < _vector.size(); ++i){
            for(unsigned int j = 0; j < _vector.size(); ++j){
                std::cout << _vector[i][j] << " ";
            }
            std::cout << std::endl;
        }
    }

}  // namespace pamsi
