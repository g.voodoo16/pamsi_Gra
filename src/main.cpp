/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File: test.cpp
 * Author: Artur Gebicz
 * (@) 2015
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "Tile.h"
#include "GameEngine.h"
#include "Array2D.h"

using namespace std;

int main(){

    GameEngine engine;

    //engine.initialize();
    engine.fast_init();

    engine.startGame();

/*
    pamsi::Array2D<int> a1(2, 2);

    // A1 TEST
    for( unsigned int i = 0; i < a1.get_height(); ++i ){
        for( unsigned int j = 0; j < a1.get_width(); ++j ){
            a1.at(i, j) = j+4;
        }

    }
    for( unsigned int i = 0; i < a1.get_height(); ++i ){
        for( unsigned int j = 0; j < a1.get_width(); ++j ){
            std::cout << a1.at(i, j) << " ";
        }
        std::cout << endl;
    }
    std::cout << endl;
*/


    return 0;
}